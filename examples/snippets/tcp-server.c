/* tcp-server.c - TCP server example.

   Copyright (c) 2015, Monaco F. J. <monaco@usp.br>

   This file is part of POSIX.

   POSIX is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <posixeg/debug.h>

#include <stdio.h>		
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>		
#include <sys/socket.h>
#include <arpa/inet.h>          /* htons */



#define PORT 5555		/* A TCP server listen to a port. */
#define BUFF_SIZE 1024

int
main (int argc, char **argv)
{
  int fd, new_fd;			
  struct sockaddr_in server_name, client_name;
  int rs;
  char buffer[BUFF_SIZE];
  size_t size;
  
  /* Create a socket. 

   Socket is a generic IPC mechanism which can be used to communicate two
   processes either locally (e.g. using main memory to send and receive 
   messages between processes in the same host) or across the network 
   (e.g. using netwrok subsystem to send and receive messages between 
   processes in different hosts). 

   When creating a socket we need to specify which kind of socket we want.
   This is called socket namespace. For instance, AC_LOCAL is a local
   socket and AF_INET is an Internet socket. 

   If we select an Internet socket we still need to specify if it's a 
   stream-type socket (ordered and confirmed transmission) or a datagram
   socket (no guarantees).

   Finally, for an Internet socket we should also select the protocol, 
   or stick with the default for a particular namespace and stream/datagram
   combination.

   In the example bellow, we create an IPv4 socket (AF_INET), stream-type
   (SOCK_STREAM) using TCP (0 is the default for stream, for datagram
   the deafult would be UDP).

   A socket gives access directly to the 4th layer of the network layered
   architecture (both in OSI and Internet jargom). The function socket()
   returns a file descriptor. 

*/
  
  fd = socket (AF_INET, SOCK_STREAM, 0);
  sysfatal (fd<0);
  
  /* So far we have only created a socket but it is not ready to be used.
     A TCP server must be bound to a network-layer port and, optionally
     to a network interface (or any of the available). */
  

  server_name.sin_family = AF_INET;                  /* Internet family */
  server_name.sin_port = htons (PORT);               /* Port to listen to */
  server_name.sin_addr.s_addr = htonl (INADDR_ANY);  /* Interface to listen to*/

  /* Ok, we have the socket and a network name. Now we bind the socket to 
     the network port and interface. */
  
  rs = bind (fd, (struct sockaddr *) &server_name, sizeof (server_name));
  sysfatal (rs<0);
  
  /* We're all done, so we can start to listen to the TCP port. 
     We'll stay here until someone tries to connect.*/


  rs = listen (fd, 1);
  sysfatal (rs<0);

  /* Ops, an incoming connection. We shall accept it. 
     Since we often want a TCP server to serve multiple client, the accept()
     function accepts the connection and create a new socket for the
     incoming client, leaving the original socket available to accept new
     connection requests.*/

  size = sizeof(client_name);
  new_fd = accept (fd, (struct sockaddr *) &client_name, (socklen_t*) &size);
  sysfatal (new_fd <0);
  

  fprintf (stderr,"Client connected from host %s, port %u\n",
	   inet_ntoa (client_name.sin_addr),
	   ntohs (client_name.sin_port));
  

  /* Ok, we can now read from the socket. */
  
  rs = read (new_fd, buffer, BUFF_SIZE-1);
  sysfatal (rs<0);
  buffer[rs] = '\0';

  printf ("I got %s\n", buffer);

  close (fd);			/* Let's close the socket. */
  
  return EXIT_FAILURE;
}
