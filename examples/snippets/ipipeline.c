#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

#include <posixeg/debug.h>


#define N 3
#define NUM_COMMANDS 3



int main ()
{
  int pid, i, j, fd[N-1][2], status;
  
/* Vector of argvs for: ls -l | grep \\.c | wc -l */

  char *args[N][NUM_COMMANDS] = 
    {
      {"ls", "-1", NULL},
      {"grep", "\\.c$", NULL},
      {"wc", "-l", NULL}
    };

/* Create N-1 pipes. */
  for (i=0; i<N-1; i++)
    pipe(fd[i]);
	

  /* Fork N processes. Note that, only the parent iterates through the loop,
     while childs immediately leave the loop and procedd.*/

  i=0;
  while ( (i<N) && (pid=fork()) )
    i++;

  /* In the parent only. */

  if (pid>0)
    {
      /* Close all pipes. */

      for (j=0; j<N-1; j++)
	{
	  close (fd[j][0]);
	  close (fd[j][1]);
	}

      /* Wait for the last subprocess. */
 
      waitpid (pid, &status, 0);

    }

  /* In each subprocess. */
  
  if (pid==0)
    {
      
      /* If I'm the first process in the pipeline */
      if(i==0){
			
	/* Close the "read" end of the first pipe */
	close (fd[i][0]);
			
	/* Redirect my output to the "write" end of the first pipe */
	close (1);
	dup(fd[i][1]);
	close (fd[i][1]);

			
	/* Close both ends of all the pipes I won't use, i.e. all the pipes 
	   whose indexes are greater than my own */
	for(j=1; j<N-1; j++){
	  close (fd[j][0]);
	  close (fd[j][1]);
	}
      }

      /* If I'm the last process in the pipeline */
      else if(i==N-1){


	/* Close the "write" end of the last pipe */
	close (fd[i-1][1]);

	/* Redirect my input to the "read" end of the last pipe */
	close (0);
	dup(fd[i-1][0]);
	close (fd[i-1][0]);

	/* Close both ends of all the pipes I won't use, i.e. all the pipes 
	   except the last one */
	for(j=0; j<N-2; j++){
	  close (fd[j][0]);
	  close (fd[j][1]);
	}

      }

      /* If I'm any process other than the first or the last */
      else{

	/* Redirect my input to the "read" end of the proper pipe, i.e. the 
	   pipe that has its index equal to mine minus 1 */
	close (0);
	dup(fd[i-1][0]);

	/* Redirect my input to the "write" end of the proper pipe, i.e. 
	   the pipe that has its index equal to mine */
	close (1);
	dup(fd[i][1]);
			
	/* Close all the ends of all the pipes */
	for(j=0; j<N-1; j++){
	  close (fd[j][0]);
	  close (fd[j][1]);
	}

      }
		
      /* Run the command I need to */
      execvp (args[i][0], args[i]);
    }

  return EXIT_SUCCESS;
}
